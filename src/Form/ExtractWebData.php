<?php

namespace Drupal\web_extract_data\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class web_extract_dataForm.
 *
 * @package Drupal\web_extract_data\Form
 */
class ExtractWebData extends FormBase {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs.
   *
   * @param Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The file system service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('entity_type.manager')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'web_extract_data_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['upload_csv'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Upload a CSV file to generate the report'),
      '#upload_location' => 'public://tmp/',
      '#default_value' => '',
      '#required' => TRUE,
      "#upload_validators"  => ["file_validate_extensions" => ["csv"]],
    ];

    $create_m_options = [
      0 => '--Select--',
      'image_extract' => 'Image extract',
      'url_extract' => 'URL extract',
      'meta_extract' => 'Meta extract',

    ];

    $form['extract_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Select Report Type'),
      '#required' => TRUE,
      '#options' => $create_m_options,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Submit',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $csv_file = $form_state->getValue('upload_csv');
    $file = $this->entityTypeManager->getStorage('file')->load($csv_file[0]);
    $file->setPermanent();
    $file->save();
    $data = $this->csvtoarray($file->getFileUri());
    $batch = [

      'title' => $this->t('Importing URL Contents...'),

      'operations' => [],

      'finished' => [static::class, 'importUrlFinished'],

    ];

    if ($data) {

      for ($i = 0; $i < count($data); $i++) {

        $batch['operations'][] = [
          [static::class, 'handleExtractType'],
          [$form_state->getValue('extract_type'), $data[$i]],
        ];

      }
      // dd($batch);
      batch_set($batch);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function handleExtractType($extract_type, $uri, &$context) {

    $output = [];
    $host = \Drupal::request()->getSchemeAndHttpHost();

    $api = \Drupal::service('web_extract_data.import');
    $result = $api->coreCall($uri);
    if ($result) {
      $response_doc = new \DOMDocument();
      $response_doc->loadHTML($result);
      if ($extract_type == 'image_extract') {
        // Like 'img' or 'table', to extract other tags.
        $imgtag = $response_doc->getElementsByTagName('img');
        // Iterate over the extracted links and display their URLs.
        $output[] = [$uri, '', '', '', ''];
        foreach ($imgtag as $link) {
          $img_source = $link->getAttribute('src') ? $link->getAttribute('src') : $link->getAttribute('data-src');
          $output[] = [
            '', $host . $img_source,
            $link->getAttribute('alt'),
            $link->getAttribute('title'),
            '',
          ];
        }
      }

      if ($extract_type == 'url_extract') {
        $tags = $response_doc->getElementsByTagName('a');
        $output[] = [$uri, '', '', ''];
        foreach ($tags as $tag) {
          $output[] = [
            '',
            $tag->getAttribute('href'),
            $tag->getAttribute('title'),
            $tag->nodeValue,
          ];
        }
      }
      if ($extract_type == 'meta_extract') {
        $tags = $response_doc->getElementsByTagName('meta');
        $output[] = [$uri, '', ''];
        foreach ($tags as $tag) {
          $output[] = [
            '',
            $tag->getAttribute('name'),
            $tag->getAttribute('content'),
          ];
        }
      }
    }

    $context['success'] = TRUE;
    $context['results'][] = $output;
    $context['results']['extract_type'] = $extract_type;
  }

  /**
   * {@inheritdoc}
   */
  protected function csvtoarray($filename, $delimiter = ',') {

    if (!file_exists($filename) || !is_readable($filename)) {
      return FALSE;
    }

    $header = FALSE;
    $data = [];

    if (($handle = fopen($filename, 'r')) !== FALSE) {
      while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
        if (!$header) {
          $header = $row;
        }
        else {
          $data[] = $row[0];
        }
      }
      fclose($handle);
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public static function importUrlFinished($success, $results, $operations) {

    $extract_type = $results['extract_type'];
    unset($results['extract_type']);
    if ($success) {
      $web_extract_data = \Drupal::service('web_extract_data.import');
      if ($extract_type == 'image_extract') {
        $web_extract_data->exportImageReport($results, $operations);
      }
      if ($extract_type == 'url_extract') {
        $web_extract_data->exportUrlReport($results, $operations);
      }
      if ($extract_type == 'meta_extract') {
        $web_extract_data->exportMetaReport($results, $operations);
      }
      $link = Link::createFromRoute('Download Report', 'web_extract_data.export', ['type' => $extract_type])
        ->toString()
        ->getGeneratedLink();
      $link_text = t("Your report is successfully generated, To download this report, Please refer to this @link", ['@link' => $link]);
      \Drupal::messenger()->addMessage($link_text, 'status', TRUE);

    }
    else {
      \Drupal::messenger()->addError(t('Something went wrong.'), 'status', TRUE);
    }

  }

}
