<?php

namespace Drupal\web_extract_data\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Messenger\MessengerInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use PhpOffice\PhpSpreadsheet\IOFactory as io_factory;
use PhpOffice\PhpSpreadsheet\Spreadsheet as spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Fill as fill;
use Psr\Log\LoggerInterface;

/**
 * Class import service will for the export csv.
 */
class ImportManagerService {

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Psr\Log\LoggerInterface definition.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  private $configFactory;

  /**
   * The Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;
  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a new ImportManagerService object.
   */
  public function __construct(MessengerInterface $messenger, ClientInterface $http_client, LoggerInterface $logger, ConfigFactoryInterface $configFactory, FileSystemInterface $fileSystem) {
    $this->messenger = $messenger;
    $this->httpClient = $http_client;
    $this->logger = $logger;
    $this->configFactory = $configFactory;
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public function makeCall($url) {
    try {
      $response = $this->httpClient->get(
            $url, [
              'verify' => FALSE,
              'headers' => [],
            ]
        );
      return $response->getBody()->getContents();
    }
    catch (ClientException $e) {
      $this->logger->error($e->getMessage());

      $this->messenger->addError($this->t('Something went wrong.'));
    }
    catch (ServerException $e) {
      $this->logger->error($e->getMessage());

      $this->messenger->addError($this->t('Something went wrong.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function coreCall($url) {
    try {
      $curl = curl_init();
      curl_setopt_array(
            $curl, [
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => TRUE,
              CURLOPT_ENCODING => '',
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 0,
              CURLOPT_FOLLOWLOCATION => TRUE,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => 'GET',
              CURLOPT_HTTPHEADER => [],

            ]
        );
      $response = curl_exec($curl);
      curl_close($curl);
      return $response;
    }
    catch (ClientException $e) {
      $this->logger->error($e->getMessage());

      $this->messenger->addError($this->t('Something went wrong.'));
    }
    catch (ServerException $e) {
      $this->logger->error($e->getMessage());

      $this->messenger->addError($this->t('Something went wrong.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function exportImageReport($results, $operations) {
    $directory = $this->fileSystem->realpath('public://tmp/');
    $objPHPExcel = new spreadsheet();
    $objPHPExcel->setActiveSheetIndex(0);
    $indexing = 1;
    if ($indexing == 1) {
      $header = ['Parent Url', 'Image Url',
        'Alternate Text', 'Title', 'Thumbnail',
      ];
      $objPHPExcel->getActiveSheet()->setCellValue('A' . $indexing, $header[0]);
      $objPHPExcel->getActiveSheet()->setCellValue('B' . $indexing, $header[1]);
      $objPHPExcel->getActiveSheet()->setCellValue('C' . $indexing, $header[2]);
      $objPHPExcel->getActiveSheet()->setCellValue('D' . $indexing, $header[3]);
      $objPHPExcel->getActiveSheet()->setCellValue('E' . $indexing, $header[4]);
      $objPHPExcel->getActiveSheet()
        ->getStyle('A' . $indexing . ':E' . $indexing)
        ->getFill()
        ->setFillType(fill::FILL_SOLID)
        ->getStartColor()
        ->setARGB('2494db');
      $objPHPExcel->getActiveSheet()
        ->getStyle('B' . $indexing)
        ->getBorders()
        ->getOutline()
        ->setBorderStyle(Border::BORDER_THIN)
        ->setColor(new Color('000000'));
      $objPHPExcel->getActiveSheet()
        ->getStyle('C' . $indexing)
        ->getBorders()
        ->getOutline()
        ->setBorderStyle(Border::BORDER_THIN)
        ->setColor(new Color('000000'));
      $objPHPExcel->getActiveSheet()
        ->getStyle('D' . $indexing)
        ->getBorders()
        ->getOutline()
        ->setBorderStyle(Border::BORDER_THIN)
        ->setColor(new Color('000000'));
      $objPHPExcel->getActiveSheet()
        ->getStyle('E' . $indexing)
        ->getBorders()
        ->getOutline()
        ->setBorderStyle(Border::BORDER_THIN)
        ->setColor(new Color('000000'));
      ++$indexing;
    }
    foreach ($results as $result) {
      foreach ($result as $row) {
        $extension = substr($row[1], strrpos($row[1], '.') + 1);
        if ($extension == 'svg') {
          continue;
        }
        $objPHPExcel->getActiveSheet()->setCellValue('E' . $indexing, '');
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $indexing, $row[0]);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $indexing, $row[1]);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $indexing, $row[2] ? $row[2] : '');
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $indexing, $row[3] ? $row[3] : '');

        if ($row[0]) {
          $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $indexing . ':E' . $indexing)
            ->getFill()
            ->setFillType(fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('87ceeb');
          $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $indexing)
            ->getBorders()
            ->getOutline()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('000000'));
        }
        else {
          $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $indexing . ':E' . $indexing)
            ->getFill()
            ->setFillType(fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('d8edfb');

          $objPHPExcel->getActiveSheet()
            ->getStyle('B' . $indexing)
            ->getBorders()
            ->getOutline()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('000000'));
          $objPHPExcel->getActiveSheet()
            ->getStyle('C' . $indexing)
            ->getBorders()
            ->getOutline()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('000000'));
          $objPHPExcel->getActiveSheet()
            ->getStyle('D' . $indexing)
            ->getBorders()
            ->getOutline()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('000000'));
          $objPHPExcel->getActiveSheet()
            ->getStyle('E' . $indexing)
            ->getBorders()
            ->getOutline()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('000000'));
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
        $objPHPExcel->getActiveSheet()->getRowDimension($indexing)->setRowHeight(50);
        ++$indexing;
      }
    }
    $objWriter = io_factory::createWriter($objPHPExcel, 'Xlsx');

    $filename = 'image-report.xlsx';
    $uri = $directory . '/' . $filename;
    $objWriter->save($uri);
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function exportUrlReport($results, $operations) {
    $directory = $this->fileSystem->realpath('public://tmp/');
    $objPHPExcel = new spreadsheet();
    $objPHPExcel->setActiveSheetIndex(0);
    $indexing = 1;
    if ($indexing == 1) {
      $header = ['Parent Url', 'Url', 'Title', 'Text'];
      $objPHPExcel->getActiveSheet()->setCellValue('A' . $indexing, $header[0]);
      $objPHPExcel->getActiveSheet()->setCellValue('B' . $indexing, $header[1]);
      $objPHPExcel->getActiveSheet()->setCellValue('C' . $indexing, $header[2]);
      $objPHPExcel->getActiveSheet()->setCellValue('D' . $indexing, $header[3]);
      $objPHPExcel->getActiveSheet()
        ->getStyle('A' . $indexing . ':D' . $indexing)
        ->getFill()
        ->setFillType(fill::FILL_SOLID)
        ->getStartColor()
        ->setARGB('2494db');
      $objPHPExcel->getActiveSheet()
        ->getStyle('B' . $indexing)
        ->getBorders()
        ->getOutline()
        ->setBorderStyle(Border::BORDER_THIN)
        ->setColor(new Color('000000'));
      $objPHPExcel->getActiveSheet()
        ->getStyle('C' . $indexing)
        ->getBorders()
        ->getOutline()
        ->setBorderStyle(Border::BORDER_THIN)
        ->setColor(new Color('000000'));
      $objPHPExcel->getActiveSheet()
        ->getStyle('D' . $indexing)
        ->getBorders()
        ->getOutline()
        ->setBorderStyle(Border::BORDER_THIN)
        ->setColor(new Color('000000'));
      ++$indexing;
    }
    foreach ($results as $result) {
      foreach ($result as $row) {
        $objPHPExcel->getActiveSheet()->setCellValue('E' . $indexing, '');
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $indexing, $row[0]);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $indexing, $row[1]);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $indexing, $row[2] ? $row[2] : '');
        $objPHPExcel->getActiveSheet()->setCellValue('D' . $indexing, $row[3] ? $row[3] : '');

        if ($row[0]) {
          $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $indexing . ':D' . $indexing)
            ->getFill()
            ->setFillType(fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('87ceeb');
          $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $indexing)
            ->getBorders()
            ->getOutline()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('000000'));
        }
        else {
          $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $indexing . ':D' . $indexing)
            ->getFill()
            ->setFillType(fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('d8edfb');

          $objPHPExcel->getActiveSheet()
            ->getStyle('B' . $indexing)
            ->getBorders()
            ->getOutline()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('000000'));
          $objPHPExcel->getActiveSheet()
            ->getStyle('C' . $indexing)
            ->getBorders()
            ->getOutline()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('000000'));
          $objPHPExcel->getActiveSheet()
            ->getStyle('D' . $indexing)
            ->getBorders()
            ->getOutline()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('000000'));
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(TRUE);
        $objPHPExcel->getActiveSheet()->getRowDimension($indexing)->setRowHeight(50);
        ++$indexing;
      }
    }
    $objWriter = io_factory::createWriter($objPHPExcel, 'Xlsx');

    $filename = 'url-report.xlsx';
    $uri = $directory . '/' . $filename;
    $objWriter->save($uri);
    return $results;
  }

  /**
   * {@inheritdoc}
   */
  public function exportMetaReport($results, $operations) {
    $directory = $this->fileSystem->realpath('public://tmp/');
    $objPHPExcel = new spreadsheet();
    $objPHPExcel->setActiveSheetIndex(0);
    $indexing = 1;
    if ($indexing == 1) {
      $header = ['Parent Url', 'Name/Propert', 'Content'];
      $objPHPExcel->getActiveSheet()->setCellValue('A' . $indexing, $header[0]);
      $objPHPExcel->getActiveSheet()->setCellValue('B' . $indexing, $header[1]);
      $objPHPExcel->getActiveSheet()->setCellValue('C' . $indexing, $header[2]);

      $objPHPExcel->getActiveSheet()
        ->getStyle('A' . $indexing . ':C' . $indexing)
        ->getFill()
        ->setFillType(fill::FILL_SOLID)
        ->getStartColor()
        ->setARGB('2494db');
      $objPHPExcel->getActiveSheet()
        ->getStyle('B' . $indexing)
        ->getBorders()
        ->getOutline()
        ->setBorderStyle(Border::BORDER_THIN)
        ->setColor(new Color('000000'));
      $objPHPExcel->getActiveSheet()
        ->getStyle('C' . $indexing)
        ->getBorders()
        ->getOutline()
        ->setBorderStyle(Border::BORDER_THIN)
        ->setColor(new Color('000000'));

      ++$indexing;
    }
    foreach ($results as $result) {
      foreach ($result as $row) {
        $objPHPExcel->getActiveSheet()->setCellValue('A' . $indexing, $row[0]);
        $objPHPExcel->getActiveSheet()->setCellValue('B' . $indexing, $row[1]);
        $objPHPExcel->getActiveSheet()->setCellValue('C' . $indexing, $row[2] ? $row[2] : '');

        if ($row[0]) {
          $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $indexing . ':E' . $indexing)
            ->getFill()
            ->setFillType(fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('87ceeb');
          $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $indexing)
            ->getBorders()
            ->getOutline()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('000000'));
        }
        else {
          $objPHPExcel->getActiveSheet()
            ->getStyle('A' . $indexing . ':E' . $indexing)
            ->getFill()
            ->setFillType(fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('d8edfb');

          $objPHPExcel->getActiveSheet()
            ->getStyle('B' . $indexing)
            ->getBorders()
            ->getOutline()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('000000'));
          $objPHPExcel->getActiveSheet()
            ->getStyle('C' . $indexing)
            ->getBorders()
            ->getOutline()
            ->setBorderStyle(Border::BORDER_THIN)
            ->setColor(new Color('000000'));
        }
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(TRUE);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(TRUE);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(TRUE);
        $objPHPExcel->getActiveSheet()->getRowDimension($indexing)->setRowHeight(50);
        ++$indexing;
      }
    }
    $objWriter = io_factory::createWriter($objPHPExcel, 'Xlsx');

    $filename = 'seo-meta-report.xlsx';
    $uri = $directory . '/' . $filename;
    $objWriter->save($uri);
    return $results;
  }

}
