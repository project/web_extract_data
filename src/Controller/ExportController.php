<?php

namespace Drupal\web_extract_data\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\File\FileSystemInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Export Controller will use for export csv.
 *
 * @package Drupal\web_extract_data\Controller
 */
class ExportController extends ControllerBase {

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   The file system service.
   */
  public function __construct(FileSystemInterface $file_system) {
    $this->fileSystem = $file_system;
  }

  /**
   * Container.
   */
  public static function create(ContainerInterface $container) {
    return new static(
          $container->get('file_system')
      );
  }

  /**
   * {@inheritdoc}
   */
  public function index($type) {
    try {

      $path = $this->fileSystem->realpath("public://tmp/");
      if ($type == 'image_extract') {
        $filename = 'image-report.xlsx';
      }
      if ($type == 'url_extract') {
        $filename = 'url-report.xlsx';
      }
      if ($type == 'meta_extract') {
        $filename = 'seo-meta-report.xlsx';
      }

      $headers = [
        'Content-Type' => 'text/xlsx',
        'Content-Description' => 'File Download',
        'Content-Disposition' => 'attachment; filename=' . $filename,
      ];

      $uri = $path . '/' . $filename;

      return new BinaryFileResponse($uri, 200, $headers, TRUE);
    }
    catch (FileNotFoundException $e) {
      return new Response('Something went wrong.');
    }
    catch (ClientException $e) {
      return new Response('Something went wrong.');
    }
    catch (ServerException $e) {
      return new Response('Something went wrong.');
    }
  }

}
